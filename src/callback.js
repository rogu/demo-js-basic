(function () {
    const callbackBox = document.querySelector('.callback-box');

    function render(container, data) {
        container.innerHTML = `
            ${data.map((item) => {
                return `<div>${item.name}</div>`
            }).join('')}
        `
    }

    function getDataWithDelay(cb) {
        setTimeout(() => {
            const data = [
                { name: 'x' },
                { name: 'y' },
                { name: 'z' }
            ];
            cb(data);
        }, 2000);
    }

    getDataWithDelay((data) => {
        render(callbackBox, data);
    })

})();
