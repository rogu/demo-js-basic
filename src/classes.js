(function () {
    const boxEl = document.querySelector('.classes-box');
    const containerEl = boxEl.querySelector('.list');

    const person1 = new Person('John', 33);
    const person2 = new Person('Nina', 33);

    class DOMRender {
        constructor(container) {
            this.container = container;
        }
        render(data) {
            const el = document.createElement('li');
            el.innerHTML = data.name;
            this.container.appendChild(el);
        }
    }

    const domRender = new DOMRender(containerEl);
    domRender.render(person1);
    domRender.render(person2);
})();
