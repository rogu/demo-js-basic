(function () {

    const boxEl = document.querySelector('.array-box');

    let people = [
        { name: 'Muhamed', age: 22 },
        { name: 'Mike', age: 33 },
        { name: 'Lenox', age: 44 },
        { name: 'Władimir', age: 55 }
    ];

    for (let index = 0; index < people.length; index++) {
        const element = people[index];
        console.log(element);
    }

    /**
     * render all
     */
    people.forEach((person) => boxEl.innerHTML += `<div>${person.name}</div>`)

    /**
     * find elements which first letter is M
     */
    const peopleStartWithM = people.filter((person) => {
        return person.name.startsWith('M');
    })

    console.log(peopleStartWithM);

    /**
     * change object in array by adding current data
     */

    const withDate = people.map((person) => {
        return { ...person, date: Date.now() }
    })

    console.log(withDate);

})();
