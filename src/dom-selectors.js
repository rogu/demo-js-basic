(function () {
    /**
     * body
     */
    document.body.style.background = '#999999'

    /**
     * selectors: id, class, attr
     */
    const domBox = document.querySelector('#dom-box');
    const list = domBox.querySelector('.list');
    const listItems = list.querySelectorAll('li');
    const input = domBox.querySelector('input[type=text]');

    /**
     * css class
     */
    input.classList.add('form-control', 'bg-primary', 'm-2');

    /**
     * css style
     */
    input.style.color = 'yellow';
    Array.from(listItems).forEach((li) => {
        li.style.border = '1px solid red';
        li.style.padding = '5px';
        li.style.margin = '5px';
    })

})();
