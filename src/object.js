(function () {
    const boxEl = document.querySelector('.object-box');

    const person = {
        name: 'Joe',
        age: 33,
        sayName() {
            alert("Hi, I'm " + this.name);
        }
    }

    boxEl.innerHTML = JSON.stringify(person, null, 4) + ' - click me!';
    boxEl.addEventListener('click', (evt) => person.sayName());

})();
