(function () {
    const eventsBox = document.querySelector('.events-box');
    const eventsInput = document.querySelector('.events-input');

    let people = [
        new Person('Muhamed', 22),
        new Person('Mike', 33),
        new Person('Lenox', 44),
        new Person('Władimir', 55)
    ];

    eventsInput.addEventListener('input', (event) => {
        const wanted = event.target.value.toUpperCase();
        const filteredCollection = people.filter((item) => {
            return item.name.toUpperCase().includes(wanted);
        })
        renderList(filteredCollection);
    })

    function renderList(collection) {
        eventsBox.innerHTML = '';
        collection.forEach((item) => {
            const el = document.createElement('li');
            el.innerHTML = item.name;
            el.addEventListener('click', event => alert(item.getName()));
            eventsBox.appendChild(el);
        });

        // event listener added to container
        eventsBox.addEventListener('mousemove', (evt) => {
            Array.from(eventsBox.querySelectorAll('li')).forEach((item) => item.style.background = 'white');
            evt.target.tagName === 'LI' && (evt.target.style.background = 'yellow');
        })
    }

    renderList(people);

})();
