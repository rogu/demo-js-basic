(function () {

    const boxEl = document.querySelector('.time-box');

    function displayCurrentTime(value) {
        boxEl.innerHTML += value + ' - ' + new Date().toLocaleString() + '<br>';
    }

    setTimeout(() => {
        displayCurrentTime('start time');
    }, 500);


    let count = 1;
    const myInterval = setInterval(() => {
        if (count >= 3) clearTimeout(myInterval);
        displayCurrentTime('nr' + count);
        count++;
    }, 1000)

})();
