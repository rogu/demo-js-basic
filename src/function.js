(function () {
    const fnBox = document.querySelector('#fn-box');
    const result = fnBox.querySelector('.result');
    const btnJoe = fnBox.querySelector('.btn-joe');
    const btnMike = fnBox.querySelector('.btn-mike');

    // ta funkcja coś robi i coś zwraca
    function greeting(name) {
        console.log(name);
        return 'welcome ' + name.toUpperCase();
    }

    // ta funkcja coś robi ale nic nie zwraca
    function print(evt) {
        result.innerHTML = greeting(evt.target.innerHTML);
    }

    result.innerHTML = greeting('init value');

    btnJoe.addEventListener('click', print);
    btnMike.addEventListener('click', print);
})();
