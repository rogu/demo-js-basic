(function () {
    const boxEl = document.querySelector('.spread-box');
    const obj = {name: 'joe'};

    boxEl.innerHTML += '<h5>object orginał</h5>';
    boxEl.innerHTML += JSON.stringify(obj);

    boxEl.innerHTML += '<h5>object kopia</h5>';
    const objCopy = {...obj, surname: 'doe'};
    boxEl.innerHTML += JSON.stringify(objCopy);
})();
