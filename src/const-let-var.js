(function () {

    const boxEl = document.querySelector('.const-let-var-box');
    const display = boxEl.querySelector('.display');
    const list = boxEl.querySelector('.list');

    var contacts = [
        { name: "Tomasz", mobile: 1111 },
        { name: "Michał", mobile: 222222 },
        { name: "Kamil", mobile: 33333333 }
    ];

    function displayMessage(i) {
        try {
            // W JavieScript (ecmascript 5) blok pętli (czyli klamry) nie tworzą zasięgu dla zmiennej utworzonej przez "var".
            // Przy każdej iteracji wartośc zmiennej "i" zmienia się stąd podczas kliknięcia w imię zmienna "i" ma wartość 3.
            display.innerHTML = 'phone: ' + contacts[i].mobile;
        } catch (err) {
            display.innerHTML = err;
        }
    }

    function createEl(i) {
        var el = document.createElement("button");
        el.textContent = contacts[i].name;
        list.appendChild(el);
        return el;
    }

    function createSeparator(text) {
        const el = document.createElement('div');
        el.innerText = text;
        list.appendChild(el);
    }

    createSeparator('for z var');

    for (var i = 0; i < contacts.length; i++) {
        const personEl = createEl(i);
        personEl.addEventListener('click', () => {
            displayMessage(i);
        });
    }

    createSeparator('for z let');

    for (let i = 0; i < contacts.length; i++) {
        const personEl = createEl(i);
        personEl.addEventListener('click', () => {
            displayMessage(i);
        });
    }

})();
