(function () {

    function render(data) {
        const names = data.map((worker) => worker.name);
        document.querySelector('.http-box').innerHTML = JSON.stringify(names);
    }

    function getWorkers(url) {
        fetch(url)
            .then((resp) => resp.json())
            .then((value) => render(value.data))
    }

    getWorkers('https://api.debugger.pl/workers');

})();
